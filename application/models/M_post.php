<?php

class M_post extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->table = array("food" => "food", "comment" => "comment", "rating" => "rating", "temporary" => "temporary");
	}

	public function all($limit = 6)
	{
		if ($limit > 0) {
			$this->db->limit($limit);
		}
		return $this->db->order_by("food_id", "DESC")->get($this->table["food"])->result();
	}

	public function create($parama)
	{
		return $this->db->insert($this->table["food"], $parama);
	}

	public function edit($where = array())
	{
		return $this->db->get_where($this->table["food"], $where)->row();
	}

	public function update($parama, $where)
	{
		return $this->db->update($this->table["food"], $parama, $where);
	}

	public function delete($where = array())
	{
		return $this->db->delete($this->table["food"], $where);
	}

	public function saveComment($params = array())
	{
		$this->db->insert($this->table["comment"], $params);
	}

	public function getComment($where = array())
	{
		return $this->db->order_by("comment_id", "DESC")->get_where($this->table["comment"], $where)->result();
	}

	public function saveRating($params = array())
	{
		$this->db->insert($this->table["rating"], $params);
	}

	public function getRating($where = array())
	{
		$total = (int)$this->db->select("sum(rating_value) as sum")->where($where)->get($this->table["rating"])->row()->sum;
		$jumlah = (int)$this->db->where($where)->get($this->table["rating"])->num_rows();
		$average = $jumlah < 1 ? 0 : $total / $jumlah;
		return array("average" => $average, "jumlah" => $jumlah);
	}

	public function haversine($data = array())
	{
		$query = explode(" ", $data["query"]);
		$latitude1 = $data["latitude"];
		$longitude1 = $data["longitude"];

		foreach ($query as $item) {
			$this->db->or_like("food_resto", $item);
		}
		foreach ($query as $item) {
			$this->db->or_like("food_name", $item);
		}

		$getData = $this->db->get($this->table["food"])->result();
		$this->db->truncate($this->table["temporary"]);
		if (!empty($getData)) {
			foreach ($getData as $value) {
				$distance = $this->distance($latitude1, $longitude1, $value->latitude,  $value->longitude, "K");
				$resultDistance = array("user_id" => "1", "food_id" => $value->food_id, "distance" => $distance,);
				$this->db->insert($this->table["temporary"], $resultDistance);
			}
		}
	}

	function distance($lat1, $lon1, $lat2, $lon2, $unit)
	{
		if (($lat1 == $lat2) && ($lon1 == $lon2)) {
			return 0;
		} else {
			$theta = $lon1 - $lon2;
			$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
			$dist = acos($dist);
			$dist = rad2deg($dist);
			$miles = $dist * 60 * 1.1515;
			$unit = strtoupper($unit);

			if ($unit == "K") {
				return ($miles * 1.609344);
			} else if ($unit == "N") {
				return ($miles * 0.8684);
			} else {
				return $miles;
			}
		}
	}

	public function getResultDistance()
	{
		return $this->db
			->join($this->table["temporary"], $this->table["temporary"] . ".food_id=" . $this->table["food"] . ".food_id")
			->order_by($this->table["temporary"] . ".distance", "DESC")->get($this->table["food"])->result();
	}
}

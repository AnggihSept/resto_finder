<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Detail_resto extends CI_Controller {

	public function index()
	{
		$data['page'] = 'detail-resto/index';
		$this->load->view('templates/main_layout', $data);
	}

}

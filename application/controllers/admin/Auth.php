<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller
{

	public function index() {
		$data['page'] = 'admin/auth/login';
		$this->load->view('templates/main_layout', $data);
	}

	public function register() {
		$data['page'] = 'admin/auth/register';
		$this->load->view('templates/main_layout', $data);
	}

}
<?php

class Post extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model("M_post");
	}

	public function index()
	{
		$data["foods"] = $this->M_post->all();
		$data["page"] = "admin/post/index";
		$this->load->view("admin/layouts/main.php", array("data" => $data));
	}

	public function post()
	{
		$data["page"] = "admin/post/post";
		$this->load->view("admin/layouts/main.php", array("data" => $data));
	}

	public function create()
	{
		$file_name = $this->do_upload();
		try {
			if ($file_name != false) {
				$params = array(
					"food_category" => $this->input->post("food_category"),
					"food_resto" => $this->input->post("food_resto"),
					"food_name" => $this->input->post("food_name"),
					"food_price" => $this->input->post("food_price"),
					"food_address" => $this->input->post("food_address"),
					"latitude" => $this->input->post("latitude"),
					"longitude" => $this->input->post("longitude"),
					"file" => $file_name,
				);
				$this->M_post->create($params);
				$this->session->set_flashdata("success", "Berhasil tambah data");
			} else {
				$this->session->set_flashdata("success", "Gagal saat upload foto");
			}
			redirect("admin/post");
		} catch (\Exception $e) {
			$this->session->set_flashdata("error", "Gagal tambah data");
			redirect("admin/post");
		}
	}

	public function edit($id = null)
	{
		$data["edit"] = $this->M_post->edit(array("food_id" => $id));
		$data["page"] = "admin/post/post_edit";
		$this->load->view("admin/layouts/main.php", array("data" => $data));
	}

	public function update()
	{
		try {
			$file_name = $this->do_upload();
			$params = array(
				"food_category" => $this->input->post("food_category"),
				"food_name" => $this->input->post("food_name"),
				"food_price" => $this->input->post("food_price"),
				"food_address" => $this->input->post("food_address"),
				"latitude" => $this->input->post("latitude"),
				"longitude" => $this->input->post("longitude"),
			);
			if ($file_name != false) {
				$params["file"] = $file_name;
			}
			$where = array("food_id" => $this->input->post("food_id"));
			$this->M_post->update($params, $where);
			$this->session->set_flashdata("success", "Berhasil edit data");
			redirect("admin/post");
		} catch (\Exception $e) {
			$this->session->set_flashdata("error", "Gagal edit data");
			redirect("admin/post");
		}
	}

	public function delete($id = null)
	{
		try {
			$where = array("food_id" => $id);
			$this->M_post->delete($where);
			$this->session->set_flashdata("success", "Berhasil hapus data");
			redirect("admin/post");
		} catch (\Exception $e) {
			$this->session->set_flashdata("error", "Gagal hapus data");
			redirect("admin/post");
		}
	}

	public function do_upload()
	{
		try {
			$file_name = time() . $_FILES["userfile"]['name'];
			$config['upload_path'] = './uploads/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['file_name'] = $file_name;

			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if (!$this->upload->do_upload('userfile')) {
				return false;
			} else {
				return "uploads/" . $this->upload->data()['file_name'];
			}
		} catch (\Exception $e) {
			return false;
		}
	}
}

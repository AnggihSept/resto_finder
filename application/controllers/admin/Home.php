<?php

class Home extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data["page"] = "admin/home/index";
		$this->load->view("admin/layouts/main", array("data" => $data));
	}
}


<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model("M_post");
	}

	public function index()
	{
		$data['page'] = 'home/index';
		$data["post"] = $this->M_post->all(10);
		$this->load->view('templates/main_layout', $data);
	}

	public function detail_resto($id = null)
	{
		$data["detail"] = $this->M_post->edit(array("food_id" => $id));
		$data["comments"] = $this->M_post->getComment(array("food_id" => $id));
		$data["rating"] = $this->M_post->getRating(array("food_id" => $id));
		$data['page'] = 'detail-resto/index';
		$this->load->view('templates/main_layout', $data);
	}

	public function post_comment()
	{
		$params = array(
			"food_id" => $this->input->post("food_id"),
			"name" => $this->input->post("name"),
			"content" => $this->input->post("content"),
		);
		$this->M_post->saveComment($params);
		return true;
	}

	public function post_rating()
	{
		$params = array(
			"food_id" => $this->input->post("food_id"),
			"rating_value" => $this->input->post("rating_value"),
		);
		$this->M_post->saveRating($params);
		return true;
	}

	public function search()
	{
		if ($this->session->userdata("user_id") == "") {
			$this->session->set_userdata("user_id", rand());
		}
		$data = array("query" => $this->input->post("query"), "latitude" => $this->input->post("latitude"), "longitude" => $this->input->post("longitude"));
		$this->M_post->haversine($data);
		$data["resultDistance"] = $this->M_post->getResultDistance();
		$this->load->view('result-distance/index', $data);
	}
}

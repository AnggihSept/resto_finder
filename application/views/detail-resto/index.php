<!-- header section -->
<header class="header-detail">
	<section class="top-bar fixed-top">
			<span onclick="window.history.go(-1);">
				<i class="fas fa-arrow-left"></i>
			</span>
		<span class="name"><?= $detail->food_resto ?></span>
	</section>

	<img src="<?= base_url($detail->file) ?>" class="w-100" alt="">
	<div class="desc p-3">
		<p class="title mb-0"><?= $detail->food_resto ?></p>
		<span class="subtitle"><?= $detail->food_name ?></span>
		<span class="d-block mt-3"><i class="fas fa-fw fa-xs fa-map-marker-alt"></i> <?= $detail->food_address ?></span>
		<p class="bottom mb-0"><i class="fas fa-fw fa-xs fa-star"></i>
			<strong><?= round((float)$rating['average'], 1) ?></strong>
			(<?= $rating['jumlah'] ?>
			rating) &nbsp;
			<strong><?=$this->input->get("distance")?></strong></p>
	</div>
</header>

<!-- main section -->
<main class="komentar pb-5">
	<div class="container">

		<div class="rating mb-4">
			<div class="row justify-content-center">
				<p class="mb-0">Beri Rating</p>
				<span>
						<i class="fas fa-fw fa-star send-rating" id="rate1" data-rating="1"></i>
						<i class="fas fa-fw fa-star send-rating" id="rate2" data-rating="2"></i>
						<i class="fas fa-fw fa-star send-rating" id="rate3" data-rating="3"></i>
						<i class="fas fa-fw fa-star send-rating" id="rate4" data-rating="4"></i>
						<i class="fas fa-fw fa-star send-rating" id="rate5" data-rating="5"></i>
					</span>
			</div>
			<div class="feed-rating mt-1"></div>
		</div>


		<p><strong>Komentar</strong></p>

		<!-- post komentar -->
		<form class="mb-3" action="" method="post">
			<div class="form-group">
				<input type="hidden" name="food_id" value="<?= $detail->food_id ?>" required>
				<input type="text" name="name" class="form-control" placeholder="Tulis nama lengkap" required>
			</div>
			<div class="form-group">
				<textarea name="content" class="form-control" id="exampleFormControlTextarea1"
						  placeholder="Tulis komentar" rows="3"></textarea>
			</div>
			<div class="form-group">
				<span class="msg"></span>
				<button class="btn btn-primary mt-2" id="send-comment" type="submit">Kirim</button>
			</div>
		</form>

		<?php if (empty($comments)): ?>
			<div class="text-muted">&quot;Beri komentar terbaik Anda.&quot;</div>
		<?php endif; ?>
		<?php foreach ($comments as $comment): ?>
			<hr>
			<div class="list-comment mb-2">
				<p class="mb-0"><?= $comment->name ?></p>
				<small class="d-block mt-2">"<?= $comment->content ?>"</small>
				<small class="time"><span class="fa fa-time"></span><?= date("d F Y", strtotime($comment->created_at)) ?>
					&nbsp; <?= date("H:i", strtotime($comment->created_at)) ?></small>
			</div>
		<?php endforeach; ?>
	</div>
</main>

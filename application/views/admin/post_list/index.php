<header>
	<nav class="fixed-top bg-nav">
		<div class="container">
			<p class="text-white my-2" onclick="window.history.go(-1);"><i class="fas fa-angle-left"></i> &nbsp; Back</p>
		</div>
	</nav>
</header>

<main class="list-resto pt-5">
	<div class="container">
		<p>Daftar postingan anda</p>
		<div class="item mb-3">
			<a href="<?= base_url(""); ?>">
				<img src="<?= base_url(); ?>assets/images/image4.jpg" alt="">
				<div class="desc">
					<p class="mb-0">Burger King</p>
					<p class="mb-0 text-muted">Beef Burger</p>
					<p class="mb-0 text-muted">Rp 75000</p>
					<p class="jarak mt-3 mb-0"><i class="fa fa-fw fa-map-marker-alt"></i>Pacific place</p>
					<p class="jarak"><i class="fas fa-fw fa-xs fa-star"></i> 4.5 &nbsp; 0.6 km (40 min)</p>
					<span></span>
				</div>
			</a>
			<a href="<?= base_url("admin/post/edit/2"); ?>" class="btn btn-info">Edit</a>
			<a href="<?= base_url("admin/post/delete/1"); ?>" class="btn btn-danger">Delete</a>
		</div>
		<div class="item mb-3">
			<a href="<?= base_url(""); ?>">
				<img src="<?= base_url(); ?>assets/images/image4.jpg" alt="">
				<div class="desc">
					<p class="mb-0">Burger King</p>
					<p class="mb-0 text-muted">Beef Burger</p>
					<p class="mb-0 text-muted">Rp 75000</p>
					<p class="jarak mt-3 mb-0"><i class="fa fa-fw fa-map-marker-alt"></i>Pacific place</p>
					<p class="jarak"><i class="fas fa-fw fa-xs fa-star"></i> 4.5 &nbsp; 0.6 km (40 min)</p>
					<span></span>
				</div>
			</a>
			<a href="#" class="btn btn-info">Edit</a>
			<a href="#" class="btn btn-danger">Delete</a>
		</div>
	</div>
</main>

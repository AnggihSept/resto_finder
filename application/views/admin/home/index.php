<!-- header section -->
<header class="admin">
	<div class="container">
		<figure>
			<img src="<?= base_url("") ?>/assets/images/avatar1.jpg" class="mx-auto d-block" alt="avatar">
			<figcaption>
				<p class="text-center">Selamat pagi admin, Semoga hari mu menyenangkan :)</p>
			</figcaption>
		</figure>
	</div>
</header>

<main>
	<div class="container">
		<a href="<?= base_url('admin/post/post'); ?>" class="btn btn-info btn-block">TAMBAH DATA</a>
		<a href="<?= base_url('admin/post'); ?>" class="btn btn-info btn-block">LIST DATA</a>
	</div>
</main>

<footer class="mt-5 p-1">
	<small class="text-center mb-0 d-block">Powered by resto finder</small>
</footer>


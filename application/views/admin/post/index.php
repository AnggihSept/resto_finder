<!-- header section -->
<header class="post-resto pt-5">
	<nav class="fixed-top bg-nav">
		<div class="container">
			<p class="text-white my-2" onclick="window.history.go(-1);"><i class="fas fa-angle-left"></i> &nbsp;
				Back</p>
		</div>
	</nav>

	<div class="container">
		<h5 class="text-muted text-center">Post Resto Favorite Anda</h5>
		<?php if ($this->session->flashdata("success")): ?>
			<div class="alert alert-success"><?= $this->session->flashdata("success") ?></div>
		<?php endif; ?>
		<?php if ($this->session->flashdata("error")): ?>
			<div class="alert alert-success"><?= $this->session->flashdata("error") ?></div>
		<?php endif; ?>
	</div>
</header>

<!-- main section -->
<header>
	<nav class="fixed-top bg-nav">
		<div class="container">
			<p class="text-white my-2" id="back"><a href="<?= base_url("admin/home") ?>" class="text-white"><i
						class="fas fa-angle-left"></i> &ensp;Home</a> &nbsp; <a
					href="<?= base_url("admin/post/post") ?>" class="text-white float-right"><span
						class="fa fa-plus"></span> Tambah</a></p>
		</div>
	</nav>
</header>

<main class="list-resto pt-5 mb-5">
	<div class="container">
		<?php if ($this->session->flashdata("success")): ?>
			<div class="alert alert-info"><?= $this->session->flashdata("success") ?></div>
		<?php endif; ?>
		<?php if ($this->session->flashdata("error")): ?>
			<div class="alert alert-info"><?= $this->session->flashdata("error") ?></div>
		<?php endif; ?>
		<p>Daftar postingan anda</p>
		<?php foreach ($foods as $food) : ?>
			<?php $rating = $this->M_post->getRating(array("food_id" => $food->food_id)); ?>
			<div class="item mb-3">
				<a href="#">
					<img src="<?= base_url("$food->file"); ?>" alt="">
					<div class="desc">
						<p class="mb-0"><?= $food->food_resto ?></p>
						<p class="mb-0 text-muted"><?= $food->food_name ?></p>
						<p class="mb-0 text-muted">Rp <?= number_format($food->food_price, 0, ",", ".") ?></p>
						<p class="jarak mt-3 mb-0"><i
								class="fas fa-fw fa-xs fa-utensils text-muted"></i> <?= $food->food_category ?>
						</p>
						<p class="jarak"><i class="fas fa-fw fa-xs fa-star"></i> <?= $rating["average"] ?>
							dari <?= $rating["jumlah"] ?> rating
						</p>
						<p class="jarak"><i class="fa fa-fw fa-map-marker-alt"></i><?= $food->food_address ?>
						</p>
						<span></span>
					</div>
				</a>
				<a href="<?= base_url("admin/post/edit/$food->food_id"); ?>" class="btn btn-info">Edit</a>
				<a href="<?= base_url("admin/post/delete/$food->food_id"); ?>" class="btn btn-danger">Delete</a>
			</div>
		<?php endforeach; ?>
	</div>
</main>

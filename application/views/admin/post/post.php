<!-- header section -->
<header class="post-resto pt-5">
	<nav class="fixed-top bg-nav">
		<div class="container">
			<p class="text-white my-2" id="back"><a href="<?= base_url("admin/post") ?>" class="text-white"><i
						class="fas fa-angle-left"></i> &ensp;Back</a></p>

		</div>
	</nav>
	<div class="container">
		<h5 class="text-muted text-center">Post Resto Favorite Anda</h5>
	</div>
</header>

<!-- main section -->
<main class="mt-3 mb-5">
	<div class="container">
		<form action="<?= base_url("admin/post/create") ?>" method="post" enctype="multipart/form-data">
			<div class="form-group mt-4">
				<label for="">Pilih Kategori</label>
			</div>
			<div class="form-group">
				<div class="custom-control custom-radio custom-control-inline">
					<input type="radio" id="customRadioInline1" name="food_category" class="custom-control-input"
						   value="Makanan Ringan"
						   required>
					<label class="custom-control-label" for="customRadioInline1">Makanan Ringan</label>
				</div>
				<div class="custom-control custom-radio custom-control-inline">
					<input type="radio" id="customRadioInline2" name="food_category" class="custom-control-input"
						   value="Makanan Berat">
					<label class="custom-control-label" for="customRadioInline2">Makanan Berat</label>
				</div>
				<div class="custom-control custom-radio custom-control-inline">
					<input type="radio" id="customRadioInline3" name="food_category" class="custom-control-input"
						   value="Minuman Panas">
					<label class="custom-control-label" for="customRadioInline3">Minuman Panas</label>
				</div>
				<div class="custom-control custom-radio custom-control-inline">
					<input type="radio" id="customRadioInline4" name="food_category" class="custom-control-input"
						   value="Minuman Dingin">
					<label class="custom-control-label" for="customRadioInline4">Minuman Dingin</label>
				</div>
			</div>
			<div class="form-group mt-4">
				<input type="text" class="form-control" id="food_resto" name="food_resto"
					   placeholder="Masukkan nama resto" required>
			</div>
			<div class="form-group mt-4">
				<input type="text" class="form-control" id="food_name" name="food_name"
					   placeholder="Masukkan nama makanan" required>
			</div>
			<div class="form-group mt-4">
				<input type="number" class="form-control" id="food_price" name="food_price"
					   placeholder="Masukkan harga.." required>
			</div>
			<div class="form-group mt-3">
				<input type="text" class="form-control" id="food_address" name="food_address"
					   placeholder="Masukkan alamat lengkap resto" required>
			</div>
			<div class="form-group mt-3">
				<input type="text" class="form-control" id="latitude" name="latitude"
					   placeholder="Terisi otomatis saat alamat resto di temukan" readonly required>
			</div>
			<div class="form-group mt-3">
				<input type="text" class="form-control" id="longitude" name="longitude"
					   placeholder="Terisi otomatis saat alamat resto di temukan" readonly required>
			</div>
			<div class="form-group mt-3 text-center">
				<input type="file" name="userfile" class="form-control" required>
				<span class="text-muted text-center">Pilih Foto</span>
			</div>
			<div class="form-group mt-2">
				<button type="submit" class="btn btn-info btn-block mx-auto d-block mt-3">POST</button>
			</div>
		</form>
	</div>
</main>

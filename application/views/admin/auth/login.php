<main>
	<div class="container pt-5">
		<h5 class="mb-4 text-center">Selamat pagi, silahkan login</h5>
		<form action="<?= base_url("admin/auth") ?>" method="post">
			<div class="form-group">
				<input type="text" class="form-control" id="nama" name="nama"
					   placeholder="Masukkan nama.." required>
			</div>
			<div class="form-group">
				<input type="password" class="form-control" id="password" name="password"
					   placeholder="Masukkan password.." required>
			</div>
			<div class="form-group mt-2">
				<button type="submit" class="btn btn-info w-100 mx-auto d-block mt-3">LOGIN</button>
			</div>
			<!-- <a href="<?//= base_url("admin/auth/register") ?>"> <small>Belum punya akun ? silahkan register</small> </a> -->
		</form>
	</div>
</main>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="<?= base_url("") ?>/assets/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css">
	<link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="<?= base_url("") ?>/assets/css/main.css">
	<title>Resto Finder</title>
</head>
<body>

<div class="wrapper">
	<?php $this->load->view($data['page'], $data) ?>

<!--	<footer class="mt-5 p-1">-->
<!--			<small class="text-center mb-0 d-block">Powered by resto finder</small>-->
<!--		</footer>-->

</div>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"
		integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
		crossorigin="anonymous"></script>
<!--<script src="--><?//= base_url(); ?><!--assets/js/main.js"></script>-->
<?php if ($this->uri->segment(3) == "post" || $this->uri->segment(3) == "edit"): ?>
	<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAgINDzGpgwWpcZtnOLuw5DtWcrO_VUsoE&libraries=places"
			type="text/javascript"></script>
	<script type="text/javascript">
		function initialize() {
			var input = document.getElementById('food_address');
			var autocomplete = new google.maps.places.Autocomplete(input);
			var options = {
				componentRestrictions: {country: 'id'}
			};
			autocomplete.setOptions(options);
			google.maps.event.addListener(autocomplete, 'place_changed', function () {
				var place = autocomplete.getPlace();
				var lat = place.geometry.location.lat();
				var lng = place.geometry.location.lng();
				$("#latitude").val(lat);
				$("#longitude").val(lng);

			});
		}

		google.maps.event.addDomListener(window, 'load', initialize);
	</script>
<?php endif; ?>
</body>
</html>

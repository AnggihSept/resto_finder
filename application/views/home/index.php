<header class="mt-3 mb-4">
	<div class="container">
		<form action="" method="post">
			<div class="form-group mb-3">
				<h5 class="text-center text-muted">Cari Makanan Faforit Anda</h5>
			</div>
			<div class="form-group text-center" id="get-address">

			</div>
			<div class="form-group">
				<div class="input-group">
					<div class="input-group-prepend">
						<div class="input-group-text bg-transparent"><i class="fas fa-map-marker-alt"></i></div>
					</div>
					<input type="text" name="address" class="form-control" id="address"
						   placeholder="Masukkan lokasi Anda.." required>
				</div>
			</div>
			<div class="form-group">
				<div class="input-group">
					<div class="input-group-prepend">
						<div class="input-group-text bg-transparent"><i class="fas fa-utensils"></i></div>
					</div>
					<input type="text" name="resto_name" class="form-control" id="resto"
						   placeholder="Mau cari makanan apa ?" required>
				</div>
			</div>
			<div class="form-group">
				<input type="hidden" name="latitude" id="latitude" required>
				<input type="hidden" name="longitude" id="longitude" required>
			</div>
			<div class="form-group">
				<button class="btn btn-info btn-block" type="submit" id="search"><span class="fa fa-search"></span>
					<span id="text-search">Cari Sekarang</span>
				</button>
			</div>
		</form>
	</div>
</header>
<main class="list-resto">
	<div class="container get-result">

	</div>
</main>
<main class="list-resto mb-5">
	<div class="container">
		<p>Temukan resto favorite anda</p>
		<?php foreach ($post as $item): ?>
			<?php $rating = $this->M_post->getRating(array("food_id" => $item->food_id)); ?>
			<div class="item">
				<a href="<?= base_url("home/detail_resto/$item->food_id"); ?>">
					<img src="<?= base_url($item->file) ?>" alt="">
					<div class="desc">
						<p class="mb-0"><?= $item->food_resto ?></p>
						<p class="mb-0 text-muted"><?= $item->food_name ?></p>
						<p class="mb-0 text-muted">Rp <?= number_format($item->food_price, 0, ",", ".") ?></p>
						<p class="jarak text-muted"><i class="fas fa-fw fa-xs fa-star"></i> <?= $rating["average"] ?>
							dari <?= $rating["jumlah"] ?></p>
						<p class="jarak mt-0"><i class="fa fa-map-marker-alt"></i> <?= $item->food_address ?></p>
						<span></span>
					</div>
				</a>
			</div>
		<?php endforeach; ?>
	</div>
</main>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"
		integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
		crossorigin="anonymous"></script>
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAgINDzGpgwWpcZtnOLuw5DtWcrO_VUsoE&libraries=places"
		type="text/javascript"></script>
<script>
	function getLocation() {
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(showPosition);
		} else {
			alert("Geolocation is not supported by this browser.")
			// x.innerHTML = "Geolocation is not supported by this browser.";
		}
	}

	function showPosition(position) {
		var latitude = document.getElementById("latitude");
		var longitude = document.getElementById("longitude");
		var lat = latitude.value = position.coords.latitude;
		var lng = longitude.value = position.coords.longitude;

		var latlng = new google.maps.LatLng(lat, lng);
		var geocoder = geocoder = new google.maps.Geocoder();
		geocoder.geocode({'latLng': latlng}, function (results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				if (results[1]) {
					var address = document.getElementById("address").value = results[1].formatted_address;
					var getAddress = document.getElementById("get-address").innerHTML = `<span class="badge badge-info" style="font-size: 8px !important;">Aplikasi berhasil ambil kolasi Anda</span>`;
					// alert("Location: " + results[1].formatted_address);
				}
			} else {

				var getAddress = document.getElementById("get-address").innerHTML = `<span class="badge badge-warning" style="font-size: 8px !important;">Aplikasi berhasil ambil kolasi Anda</span>`;
			}
		});
		// x.innerHTML = "Latitude: " + position.coords.latitude +
		// 	"<br>Longitude: " + position.coords.longitude;
	}

	getLocation();
	// ajax search
	$("#search").on("click", function (e) {
		e.preventDefault();
		var latitude = $("#latitude").val();
		var longitude = $("#longitude").val();
		var query = $("#resto").val();
		var getResult = $(".get-result");
		if (latitude == "" || longitude == "") {
			getResult.html("<div class='text-center alert alert-warning'>Harap masukkan lokasi Anda.</div>");
			return false;
		}
		if (query == "") {
			getResult.html("<div class='text-center alert alert-warning p-1 '>Harap Masukkan Nama makanan atau minuman dengan benar.</div>");
			return false;
		}

		search(latitude, longitude, query);

	});

	function search(latitude, longitude, query) {
		$.ajax({
			url: '<?=base_url("home/search")?>',
			beforeSend: function () {
				$("#search").html(`<span class="fa fa-search"></span><span id="text-search"><i>Sedang Mencari</i></span>`);
			},
			method: 'POST',
			data: {latitude: latitude, longitude: longitude, query: query},
			typeData: 'HTML',
			success: function (data) {
				$(".get-result").html(data);
				$("#search").html(`<span class="fa fa-search"></span><span id="text-search"> Cari Sekarang</span>`);
			},
			error: function (e) {
				$(".get-result").html(`<div class='text-center alert alert-warning'>Maaf, ada ganguan dengan sistem. Harap coba kembali dan isikan alamar dengan benar</div>`);
				$("#search").html(`<span class="fa fa-search"></span><span id="text-search"> Cari Sekarang</span>`);
			}
		})
	}

	function initialize() {
		var input = document.getElementById('address');
		var autocomplete = new google.maps.places.Autocomplete(input);
		var options = {
			componentRestrictions: {country: 'id'}
		};
		autocomplete.setOptions(options);
		google.maps.event.addListener(autocomplete, 'place_changed', function () {
			var place = autocomplete.getPlace();
			var lat = place.geometry.location.lat();
			var lng = place.geometry.location.lng();
			$("#latitude").val(lat);
			$("#longitude").val(lng);

		});
	}

	google.maps.event.addDomListener(window, 'load', initialize);

</script>

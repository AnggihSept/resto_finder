<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="<?= base_url(); ?>assets/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css">
	<link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/main.css">
	<title>Resto Finder</title>
</head>
<body>


<!-- main content -->
<?php
$this->load->view($page);
?>


<section class="mt-5 p-1 text-muted">
	<small class="text-center mb-0 d-block">&copy;Powered by Resto Finder</small>
</section>

<section class="notif d-none">
	<p>Maaf, Lokasi anda tidak berhasil didapatkan</p>
</section>

<?php if ($this->uri->segment(2) == "detail_resto"): ?>
	<script src="<?= base_url(); ?>assets/js/main.js"></script>
<?php endif; ?>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"
		integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
		crossorigin="anonymous"></script>
<script>
	$("#send-comment").on("click", function (e) {
		e.preventDefault();
		var food_id = $('[name="food_id"]').val();
		var name = $('[name="name"]').val();
		var content = $('[name="content"]').val();
		if (food_id == "" || name == "" || content == "") {
			return false;
		}
		$.ajax({
			url: '<?=base_url("home/post_comment")?>',
			beforeSend: function () {
				$("#send-comment").html('<i>Mengirim..</i>');
			},
			method: 'POST',
			data: {food_id: food_id, name: name, content: content},
			dataType: "html",
			success: function (data) {
				$("form")[0].reset();
				$("#send-comment").html('Kirim');
				location.reload();
			}
		})
	});

	$(".send-rating").on("click", function (e) {
		e.preventDefault();
		var food_id = $('[name="food_id"]').val();
		var rating_value = $(this).attr("data-rating");
		$.ajax({
			url: '<?=base_url("home/post_rating")?>',
			method: 'POST',
			data: {food_id: food_id, rating_value: rating_value},
			dataType: "html",
			success: function (data) {
				$(".feed-rating").html(`<div class="alert alert-info alert-sm text-center">Terimakasih telah memberi <b>` + rating_value + `</b> rating kepada kami :)</div>`);
				setInterval(function () {
					location.reload();
				}, 2000)
			}
		})
	})
</script>
</body>
</html>

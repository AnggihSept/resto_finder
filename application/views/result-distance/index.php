<p class="text-left text-muted mb-2">Ditemukan <b><?= count($resultDistance) ?></b> data</p>
<?php if (!empty($resultDistance)): ?>
<?php endif; ?>
<?php foreach ($resultDistance as $item): ?>
	<?php $rating = $this->M_post->getRating(array("food_id" => $item->food_id)); ?>
	<div class="item">
		<a href="<?= base_url("home/detail_resto/$item->food_id")."?distance=".round($item->distance , 2)."Km"; ?>">
			<img src="<?= base_url($item->file) ?>" alt="">
			<div class="desc">
				<p class="mb-0"><?= $item->food_resto ?></p>
				<p class="mb-0 text-muted"><?= $item->food_name ?></p>
				<p class="mb-0 text-muted">Rp <?= number_format($item->food_price, 0, ",", ".") ?></p>
				<p class="jarak mt-2 text-muted"><i class="fas fa-fw fa-xs fa-star"></i> <?=$rating["average"]?> dari <?=$rating["jumlah"]?>, <b><i class="fa fa-car"></i> <?= round($item->distance , 2) ?>Km </b></p>
				<p class="jarak"><i class="fa fa-map-marker-alt"></i> <?= $item->food_address ?></p>
			</div>
		</a>
	</div>
<?php endforeach; ?>
<?php if (!empty($resultDistance)): ?>
	<hr>
<?php endif; ?>
<?php if (empty($resultDistance)): ?>
	<div class="alert alert-info">Yaah.. pencarian tidak di temukan :( , coba deh ketik kata lain yang lebih spesifik..
	</div>
<?php endif; ?>

